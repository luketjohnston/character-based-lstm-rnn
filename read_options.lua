-- File to read command line options
--

require 'torch'

getOptions = function()

  cmd = torch.CmdLine()
  cmd:text()
  cmd:text('Options:')
  -- global:
  cmd:option('-seed', 1, 'fixed input seed for repeatable experiments')
  cmd:option('-threads', 2, 'number of threads')
  cmd:option('-useCuda',1,'Set to 0 if you want to run on CPU.')
  -- filenames:
  cmd:option('-save', 'results', 'subdirectory to save/log experiments in')
  cmd:option('-model','nil','Filename of saved model')
  cmd:option('-data','data/test.txt','Filename of the text datafile.')
  -- preferences
  cmd:option('-valStep',100,'Number of batches in between validation calculations.')
  cmd:option('-temperature',1,'Temperature to sample the model with.')
  cmd:option('-val',0.001,'Proportion of the dataset to use for validation.')
  -- training parameters:
  cmd:option('-learningRate', 2e-3, 'learning rate at t=0')
  cmd:option('-seqLength', 50, 'mini-batch size (1 = pure stochastic)')
  cmd:option('-dropout',0.5,'Dropout')
  cmd:option('-batchSize',1,'Number of sequences to train on in parallel.')
  cmd:option('-gradClip',5,'Clip gradient magnitude below this value.')
  cmd:text()
  opt = cmd:parse(arg or {})
  return opt
end
