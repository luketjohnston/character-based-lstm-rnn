require 'torch'   -- torch
require 'xlua'    -- xlua provides useful tools, like progress bars
require 'optim'   -- an optimization package, for online and batch methods
require 'nn'
require 'rnn'
if opt.useCuda==1 then require 'cunn' end
-- Defines functions to create and use minibatches.
require 'minibatch.lua' 
-- contains function to sample from the model (generate text with model).
require 'sample.lua'

----------------------------------------------------------------------
print '==> configuring optimizer'

optimState = {
   learningRate = opt.learningRate,
   --weightDecay = opt.weightDecay,
   --momentum = opt.momentum,
   alpha = 0.95
}
optimMethod = optim.rmsprop

----------------------------------------------------------------------
print '==> defining training procedure'

-- does one epoch on the training data
function train()

  -- epoch tracker
  epoch = epoch or 1


  -- local vars
  local time = sys.clock()

  -- set model to training mode (for modules that differ in training and testing, like Dropout)
  model:training()

  -- do one epoch
  print('==> doing epoch on training data:')
  print("==> online epoch # " .. epoch .. ' [seqLength = ' .. opt.seqLength .. ']')

  -- t will index the start of this batch.
  for t = 1,data.endTrain,(opt.seqLength*opt.batchSize) do

     batch = (batch or 0) + 1
     -- if necessary, calculate validation loss and save best model
     if batch % opt.valStep == 0 then
       print('Calculating validation loss...')
       local loss = validate()
       print('Validation loss: ' .. loss)
       losses = losses or {}
       table.insert(losses, loss)
       if (not min_loss) or (loss < min_loss) then
         min_loss = loss
         print('==> This is the best validation loss so far. Saving...')
         torch.save(opt.save .. '/model.net',model)
         print('Example sample: ')
         print(sample(model,100,1))
       end
       torch.save(opt.save .. '/validation_loss_list.t7',losses)
       model:training()
     end

     -- disp progress bar
     xlua.progress(t, data.endTrain)

     inputs,targets = makebatch(data.inputs, data.targets, t, opt.seqLength,opt.batchSize)

     -- create closure that optim needs
     -- x is the parameters to update the model with for this batch. Returns
     -- the average loss, and the normalized gradParameters
     local optimClosure = function(x)
       -- Prepare model for next pass by updating parameters and zeroing
       -- gradients
       if x ~= parameters then -- I don't think this evcery actually happens...
         parameters:copy(x)
       end
       gradParameters:zero()

       -- do forward and backward pass, return loss. Also updates and
       -- normalizes gradParameters.
       local loss = evalBatch(model, criterion, inputs, targets)
       -- clip gradients
       gradParameters:clamp(-opt.gradClip, opt.gradClip)

       return loss, gradParameters
     end

     -- optimize parameters vector on current mini-batch
     optimMethod(optimClosure, parameters, optimState)
  end

  -- time taken
  time = sys.clock() - time
  time = time / data.endTrain
  print("\n==> time to learn 1 sample = " .. (time*1000) .. 'ms')



end
