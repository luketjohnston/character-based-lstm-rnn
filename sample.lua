
-- require 'vocab'
function sample(model,length,temperature)
  -- need this, because we are currently remembering stuff specific to batch 
  -- processing
  model:forget() 
  model:evaluate() -- set in evaluation mode
  s = '' -- start with empty string
  -- start with equal distribution over characters
  log_probs = torch.Tensor(1, vocab.size):fill(1)/vocab.size

  -- we predict characters randomly according to the probability distribution
  -- given by the model.
  for i=1,length do
    
    -- dividing by smaller temperature before exponentiating will emphasize
    -- more confident predictions
    log_probs:div(temperature)
    local probs = torch.exp(log_probs):squeeze()
    probs:div(torch.sum(probs)) -- renormalize so probs sum to one. Is this necessary? not for multinomial...
    -- sample from probability distribution
    prediction = torch.multinomial(probs:float(), 1):resize(1)
    s = s .. vocab.int_to_char[prediction[1]]

    -- now, forward character through the model, and save new log_probs: 
    -- need to wrap in a table for sequencer
    log_probs = model:forward({prediction})[1]
    
  end
  return s
end

  
