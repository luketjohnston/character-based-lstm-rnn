require 'rnn'
require 'nn'

dofile '1_data.lua'
print('loaded data')

cmd = torch.CmdLine()
cmd:text('Options:')
cmd:option('-length',-1,'Number of characters to sample. If -1, then endless.')
cmd:option('-model','/home/ubuntu/ec2_30/test1/model.net','filename of torch save of model.')
cmd:option('-temperature',1,'temperature for sampling.')
cmd:option('-outFile','nil','Output file')
cmd:option('-useCuda',1,'set to 0 if not using cuda')
cmd:text()
opt = cmd:parse(arg or {})

if opt.useCuda == 1 then require 'cunn' end


print('Loading model...')
local model = torch.load(opt.model)
print('Done!')
local temperature = opt.temperature
local length = opt.length




-- need this, because we are currently remembering stuff specific to batch 
-- processing
model:forget() 
model:evaluate() -- set in evaluation mode


s = '' -- start with empty string
-- start with equal distribution over characters
log_probs = torch.Tensor(1, vocab.size):fill(1)/vocab.size

-- we predict characters randomly according to the probability distribution
-- given by the model.
local i = 0
print('sampling...')
while length == -1 or i < length do
  i = i + 1
  
  -- dividing by smaller temperature before exponentiating will emphasize
  -- more confident predictions
  log_probs:div(temperature)
  local probs = torch.exp(log_probs):squeeze()
  probs:div(torch.sum(probs)) -- renormalize so probs sum to one. Is this necessary? not for multinomial...
  -- sample from probability distribution
  prediction = torch.multinomial(probs:float(), 1):resize(1)

  io.write(vocab.int_to_char[prediction[1]])
  s = s .. vocab.int_to_char[prediction[1]]
  started_song = true

  -- now, forward character through the model, and save new log_probs: 
  -- need to wrap in a table for sequencer
  log_probs = model:forward({prediction})[1]
  
end

if opt.filename ~= 'nil' then
  io.output(opt.outFile)
  io.write(s)
  io.flush()
end
  
