require 'torch'   -- torch
require 'xlua'    -- xlua provides useful tools, like progress bars
require 'optim'   -- an optimization package, for online and batch methods

----------------------------------------------------------------------
print '==> defining test procedure'
require 'rnn'
require 'nn'
if opt.useCuda==1 then require 'cunn' end


-- test function
function validate()
  -- set model to evaluate mode (for modules that differ in training and testing, like Dropout)
  model:evaluate()


  -- create validation "batch" (which is the full validation data)
  -- Don't have to do actual batch processing on the validation data, because
  -- there is no need to remember past inputs for back-propogation, and we're
  -- not updating parameters, so there's no downside to just doing the whole
  -- validation set at once.
  local seq_len = (data.size - data.endTrain) / opt.batchSize

  local inputs,targets = makebatch(data.inputs,data.targets,1,seq_len,opt.batchSize)

  outputs = model:forward(inputs)
  loss = criterion:forward(outputs, targets) / #inputs
  return loss
end

  


