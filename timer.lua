-- A function to collect statistics on how long a function takes. But I'm not
-- using it anywhere right now.

take_time = false -- set to true if you want to take time statistics

time_table = time_table or {}
print_interval = 5
time_func = function(f, unique_id)
  if take_time then
    -- global "time table" to hold stats
    time_table[unique_id] = time_table[unique_id] or {count = 0, average_time = 0}
    
    -- start the timer, then execute the function and record time taken
    timer = torch.Timer()
  end

  ret1,ret2 = f()

  if take_time then
    time = timer:time().real
    count = time_table[unique_id].count + 1
    prev_av = time_table[unique_id].average_time
    time_table[unique_id].average_time = ((prev_av * (count - 1)) + time) / count
    time_table[unique_id].count = count
    if count % print_interval == 0 then
      print('Average time of function ' .. unique_id .. ' in milliseconds: ')
      print(time_table[unique_id].average_time * 1000)
    end
  end
  return ret1, ret2
end
