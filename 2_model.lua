

require 'torch'   -- torch
require 'image'   -- for image transforms
require 'nn'      -- provides all sorts of trainable modules/layers
require 'rnn'
if opt.useCuda==1 then require 'cunn' end
require 'read_options.lua'
require '1_data.lua'

if not opt then
  opt = getOptions()
end


-- idk about this. When I try to load a model and then save it, I get memory errors...
if opt.model ~= 'nil' then 
  model = torch.load(opt.model)
  model = model:forget() -- forget past
  model:remember('both')
  parameters,gradParameters = model:getParameters()
else
  
  ----------------------------------------------------------------------
  print '==> define parameters'
  -- number of backprop steps to take in time.
  rho = 10
  -- Size of the output of the recurrent layer
  hiddenSize = 512
  
  ----------------------------------------------------------------------
  print '==> construct model'
  
  model = nn.Sequential()
  model:add(nn.Sequencer(nn.LookupTable(vocab.size,hiddenSize)))
  model:add(nn.Sequencer(nn.FastLSTM(hiddenSize,hiddenSize,rho)))
  model:add(nn.Sequencer(nn.FastLSTM(hiddenSize,hiddenSize,rho)))
  model:add(nn.Sequencer(nn.Linear(hiddenSize, vocab.size)))
  model:add(nn.Sequencer(nn.LogSoftMax()))

  if opt.useCuda==1 then
    print('pushing model to GPU')
    model:cuda()

    container = nn.Sequential()
    container:add(nn.Sequencer(nn.Copy('torch.ByteTensor','torch.CudaTensor')))
    container:add(model)

    model = container
  end
  
  
  -- so remembers previous state during both testing and training
  model:remember('both') 

  
  -- Retrieve parameters and gradients:
  -- this extracts and flattens all the trainable parameters of the model
  -- into a 1-dim vector
  parameters,gradParameters = model:getParameters()
  parameters:uniform(-0.08,0.08) -- small uniform initialization

end
----------------------------------------------------------------------
print '==> here is the model:'
print(model)

----------------------------------------------------------------------

