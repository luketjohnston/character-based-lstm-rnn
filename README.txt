This is the current development branch of the character LSTM.

Update: It's going as fast as karpathy's code, and seems to be working just as
well too! = )






To run on the complete works of shakespeare:

th doall.lua -data/shakespeare.txt

Or on the Hillary Clinton emails:

th doall.lua -data/HillaryClintonEmails.txt


If you run on one dataset and then want to run on a different dataset, you need to
delete data/data.t7 and data/vocab.t7 first.


Run

th doall.lua -h

for all available options.

If you want to change the number or size of the layers in the model, you'll need to modify 2_model.lua.



