
-- this function forwards the input through the network, and returns the
-- normalized loss. it updates and normalizes gradparameters in the process.
--
-- should have already updated the parameters for the model from last
-- optimization result, and should have already zeroed gradients from last
-- optimization result.
--
-- inputs and targets are both tables of input tensors.
evalBatch = function(model, criterion, inputs, targets)

  -- do minibatch
  --print(inputs)
  local output = model:forward(inputs)
  local loss = criterion:forward(output,targets)

  -- backward pass. accumulates gradparameters
  local df_do = criterion:backward(output, targets)
  model:backward(inputs, df_do)

  -- normalize loss over batch
  loss = loss / #inputs

  return loss

end 


-- given torch tensors of inputs and targets, a start point, and batch size,
-- returns the table of inputs and the table of targets for that minibatch.
-- use_cpu is optional, if not specified, uses CUDA.
--
-- Requires that the inputs and targets have already been processed to be a
-- multiple of (batch_size * seq_len)
makebatch = function(inputs,targets,start,seq_len,batch_size)

  -- create mini batch
  -- We do batch processing so that we can update the parameters less
  -- frequently, and the updates are more effective (less noise) => faster.
  local batch_inputs = {}
  local batch_targets = {}

  -- This table will be used to index one character from each sequence at a
  -- time, to form a single batch.
  local offsets = torch.LongTensor(batch_size)
  for i=1,batch_size do offsets[i] = start + (i-1)*seq_len end

  for i=1,seq_len do
    input = inputs:index(1,offsets) -- get batch of inputs
    target = targets:index(1,offsets) -- corresponding batch of targets
    if opt.useCuda == 1 then
      target = target:cuda()
    end 
    table.insert(batch_inputs, input) -- insert batch into sequencer input table
    table.insert(batch_targets, target)
    offsets:add(1) -- increment offsets so we index the next batch of chars.
  end
  return batch_inputs,batch_targets
end
