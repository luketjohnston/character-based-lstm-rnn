-------------------------------------------------------------------------------
-- loads opt.data into torch tensor and saves to file. Will load 
-- from this second file on subsequent calls.
-------------------------------------------------------------------------------

require 'torch'   -- torch
require 'image'   -- for color transforms
require 'nn'      -- provides a normalization operator


----------------------------------------------------------------------
print '==> loading dataset'

-- filenames of torch tensors of data and vocab
data_file = 'data/data.t7'
vocab_file = 'data/vocab.t7'

if paths.filep(vocab_file) then
  data = torch.load(data_file)
  vocab = torch.load(vocab_file)
else
  -- create tensor for data and vocab and save to file
  -- mappings from characters to ints and vica versa
  vocab = {char_to_int = {}, int_to_char = {}, size = 0}
  f = io.open(opt.data, "r")
  -- build vocab and determine length of data
  local cache_len = 10000 -- # of characters to read in at one time
  local total_characters = 0 -- total characters in whole dataset
  rawdata = f:read(cache_len) -- read line of data (one email)
  repeat
    total_characters = total_characters + rawdata:len()
    for char in rawdata:gmatch'.' do
      if not vocab.char_to_int[char] then 
        vocab.size = vocab.size + 1
        vocab.char_to_int[char] = vocab.size 
        vocab.int_to_char[vocab.size] = char
      end
    end
    rawdata = f:read(cache_len)
  until not rawdata
  f:close()

  -- want all batches to be the same size, so truncate dataset
  print(opt.batchSize)
  total_characters = total_characters - (total_characters % (opt.seqLength * opt.batchSize))
  print(total_characters)

  -- construct data tensor
  inputs = {} 
  targets = {}
  f = io.open(opt.data,"r")
  rawdata = f:read(cache_len)
  local c = 1
  -- Tensor to hold all character inputs.
  fulldata = torch.ByteTensor(total_characters) 
  repeat
    for i=1,#rawdata do
      if c <= total_characters then
        fulldata[c] = vocab.char_to_int[rawdata:sub(i,i)]
        c = c + 1
      end
    end
    rawdata = f:read(cache_len)
  until not rawdata
  f:close()

  -- determine end of training data (and beginning of validation data)
  local endTrain = math.floor((1-opt.val) * (#fulldata)[1])
  local endTrain = endTrain - (endTrain % (opt.seqLength * opt.batchSize)) -- round to batch size
  
  -- now save vocab and data tensors to file
  torch.save(vocab_file, vocab)
  data = {
    fulldata = fulldata,
    inputs = fulldata:narrow(1,1,(#fulldata)[1]-1),
    targets = fulldata:narrow(1,2,(#fulldata)[1]-1),
    size = ((#fulldata)[1]-1),
    endTrain = endTrain
  }
  torch.save(data_file, data) 

end


