require 'torch'
require 'read_options.lua'
require 'lfs' -- to make directories

----------------------------------------------------------------------
print '==> processing options'

opt = getOptions()

if opt.useCuda==1 then 
  print('==> Using CUDA') 
  require 'cunn'
else print('==> Running on the CPU') end

torch.setdefaulttensortype('torch.FloatTensor')

-- nb of threads and fixed seed (for repeatable experiments)
torch.setnumthreads(opt.threads)
torch.manualSeed(opt.seed)

-- if save directory doesn't exist, create it.
lfs.mkdir(opt.save)

----------------------------------------------------------------------
print '==> executing all'

dofile '1_data.lua'
dofile '2_model.lua'
dofile '3_loss.lua'
dofile '4_train.lua'
dofile '5_test.lua'

----------------------------------------------------------------------
print '==> training!'

epoch = epoch or 1
while true do
   train()
   epoch = epoch + 1
end
