-- Define the loss function.

require 'torch'   -- torch
require 'nn'      -- provides all sorts of loss functions

---------------------------------------------------------------------

-- Mean Squared Error
-- criterion = nn.SequencerCriterion(nn.MSECriterion())
--
criterion = nn.SequencerCriterion(nn.ClassNLLCriterion())
if opt.useCuda == 1 then
  print('pushing criterion to GPU')
  criterion:cuda()
end



----------------------------------------------------------------------
print '==> here is the loss function:'
print(criterion)
